# Ansible Role: Install Terraform

Installs [Terraform](https://www.terraform.io).

## Requirements

None.

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    terraform_version: "1.1.2" - This version should be used until Openstack version is upgraded

The Terraform version to install.

    terraform_arch: "amd64"

The system architecture (e.g. `386` or `amd64`) to use.

    terraform_bin_path: /usr/local/bin

The location where the Terraform binary will be installed (should be in system `$PATH`).

## Dependencies

None.

## Example Playbook

    - hosts: builder
      roles:
        - install-terraform
